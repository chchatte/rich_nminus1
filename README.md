#Rich n-1 extraction :
===================
Author :  Chandradoy Chatterjee
Last updated: 26/09/2023
-	The code `ExtractRefractiveIndex.sh` does almost everything. The user can check the required arguments passing NULL argument to the script.
-	The code were generated for DVCS and SIDIS runs but can be used for any run. 


##Base Codes:
===========
	### refIndex_extraction.C :
		- Extracts the refractive indices for the UV and Visible Photon Detectors (PDs).
		- The visible PDs are based on MAPMT technology : Large statistics.
			-- Global refractive index and weighted average of index of individual cathodes are considered for final index.
		- The UV PDs are gaseous detectors : Can be of limited statistics
			-- Global UV refractive index is used.
	### merge_ref_run_info.C
		- Merges the run and the time information of the run to prepare files to database and writes it in the format as was suggested by Andrea Bressan. 
 		    	 	
