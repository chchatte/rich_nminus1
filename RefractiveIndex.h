#ifndef REFRACTIVEINDEX_H
#define REFRACTIVEINDEX_H


#include <iostream>
#include <fstream>
#include <string.h>
#include <TString.h>
#include <iomanip>
/*-------------------*/

#include "TMath.h"
#include "TH1.h"
#include "TString.h"
#include "TFile.h"
#include "TDirectory.h"
using namespace std;
const int Ncathode=4;
TH1D  * global_ref;
TH1D  * global_ref_UV;
TH1D  * cath_ref[Ncathode];
TFile * rootfile;

vector<TH1D *> myHistoUV;
vector<TH1D *> myHistoVS;

//TFile * n-1_Info;
TDirectory *uvDir;
TDirectory *vsDir;

double multFactor = 1.109;
double ref_Index;
double ref_Index_cath[Ncathode];
double ref_Sigma;
double ref_Sigma_cath[Ncathode];
double ref_Index_UV; 
void init();
double extractHisto(TFile *);
double extractUVHisto(TFile *);
void writeFile();
void BookHisto(vector<TString>, vector<TH1D*>, vector<TH1D*>);
vector<TString> rn;
void readSettingsFile();
string Period;
string Production;
string ProductionPath;
string refRunList;
string refROOTFile;
string finalOutFile;
string Year;
string Week;
string runTimeFile;
int runYear;
TFile *myROOTFile;
#endif

