#!/usr/bin/perl -w
use warnings;
$num_args = $#ARGV +1 ;
if($num_args !=1){
 print "\nUsage: getridofslash.pl <P0N>";
}
$period=$ARGV[0];
my $filename = './Run_end_time.txt';
open(my $fh, '<:encoding(UTF-8)', $filename)
 or die "Could not open file '$filename' $!";

open(my $fg, '>', "./Run_end_time_$period.txt");
 while (<$fh>) {
  my $row = $_;
  chomp($row);
        	#print "$row\n";
  $row =~  s/:/ /g;
  $row =~  s/\// /g;
  $row =~  s/-/ /g;
  print $fg "$row\n";

 }
 close $fh;
 print "Done\n";
