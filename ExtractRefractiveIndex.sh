#!/bin/bash
RED='\033[0;31m'
NC='\033[0m' 
bold=$(tput bold)
normal=$(tput sgr0)
usage() { echo -e "Usage : $0 [-P <string> (P08)] [-t <string> (t5)] [-W <string> (W11)] [-Y <string> (2017)] [-F <string> (EndTimeFile)] \n ${RED}${bold}No Period needed for SIDIS${NC}${normal}" 1>&2; exit 1; }

usageDVCS() { echo "Usage (DVCS): $0 [-P <string> (P08)] [-t <string> (t5)] [-W <string> (W11)] [-Y <string> (2017)] [-F <string> (EndTimeFile)]" 1>&2; exit 1; }
usageSIDIS() { echo "Usage (SIDIS): $0 [-t <string> (t5)] [-W <string> (W11)] [-Y <string> (2017)] [-F <string> (EndTimeFile)]" 1>&2; exit 1; }

if [ "$#" -eq 0 ]; then
        usage
fi

while getopts ":P:t:W:Y:F:" o; do
    case "${o}" in
        P)
            myP=${OPTARG}
	     echo "Your Period is $myP"	  	
            ;;
        t)
            myT=${OPTARG}
	     echo "Your production is $myT"     	
            ;;
	W)
	    myWeek=${OPTARG}
	      echo "Your prod week is $myWeek"
	    ;;		
	Y)  myYear=${OPTARG}
	      echo "Your Prod year is $myYear" 	
	    ;;		
	F)  myEndFile=${OPTARG}
	      echo "Run end time listed in $myEndFile" 
	    ;;	
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
outFile=./Settings.ini
echo "# changed from Bash file">${outFile}
if [ ${myYear} -eq 2016 ] || [ ${myYear} -eq 2017 ]; then
	echo "File_P = ${myP}" >>${outFile}
	echo "Run_Ref_File = ${myP}${myT}_Run_By_Run_Final_RICH_Indices.txt">>${outFile}
	echo "FinalOutFile = ${myP}${myT}_Run_By_Run_RICH_Indices_Std_Format.txt">>${outFile}
	echo "RunTimeFile = Run_end_time_${myP}.txt">>${outFile}
	echo "Out_ROOT_File = ${myP}_RefIndex.root">>${outFile}
	extension=dvcs${myYear}${myP}${myT}
fi

if [ ${myYear} -eq 2022 ]; then
	echo "Run_Ref_File = ${myWeek}${myT}_Run_By_Run_Final_RICH_Indices.txt">>${outFile}
	echo "FinalOutFile = ${myWeek}${myT}_Run_By_Run_RICH_Indices_Std_Format.txt">>${outFile}
	echo "RunTimeFile = Run_end_time_${myWeek}.txt">>${outFile}
	echo "Out_ROOT_File = ${myWeek}_RefIndex.root">>${outFile}
	extension=transv${myYear}${myWeek}${myT}
fi

echo $extension

outFile=./Settings.ini
echo "File_t = ${myT}" >>${outFile}
echo "Week  = ${myWeek}">>${outFile}
echo "Year = Y${myYear}">>${outFile}
echo "# No change needed">>${outFile}
echo "File_Path = /eos/experiment/compass/generalprod/testcoral/">>${outFile}

perlInputFile=./Run_end_time.txt
production='/eos/experiment/compass/generalprod/testcoral/'${extension}
ls -v /eos/experiment/compass/generalprod/testcoral/${extension}/histos/*.root >test.txt
if [ -z  "$(ls -v /eos/experiment/compass/generalprod/testcoral/${extension}/histos/*.root)" ]; then
  echo -e "${RED}${bold}Error: Histo files in ${production} missing ${normal}${NC}"
  exit 1;	
fi
echo " Running Ref index extractor "
root -l -b -q refIndex_extraction.C
echo "Accessing COMPASS dB "
mysql -u coral -pna58coral -b runlb -h compassvm-userdb01 -e "select runnb, stoptime from tb_run where period = '${myYear}-${myWeek}';" >$myEndFile
tail -n +2 ${myEndFile} > ./RunTime.txt
rm $perlInputFile
python3 ReadRunList.py $perlInputFile
if [ ${myYear} -eq 2016 ] || [ ${myYear} -eq 2017 ]; then
	./getridofslash.pl $myP
fi
if [ ${myYear} -eq 2022 ]; then
	./getridofslash.pl $myWeek
fi
echo "Merging all Info "
root -l -b -q merge_ref_run_info.C 
